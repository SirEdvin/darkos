local function start()
    local voider = peripheral.find("lava_bucket")
    local sucker = peripheral.find("endAutomata")
    while true do
        sleep(2)
        if sucker ~= nil then
            sucker.suck()
        end
        for i=1, 16 do
            if turtle.getItemCount(i) > 0 then
                turtle.select(i)
                voider.void()
            end
        end
    end
end

return {
    start = start
}