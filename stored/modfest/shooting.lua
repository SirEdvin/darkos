-- dependency
-- doerlib
-- doerlib.extras:stands
-- doerlib.extras:leaderboard

local doerlib = require("doerlib")
local stands = require("doerlib.extras.stands")
local leaderboard = require("doerlib.extras.leaderboard")

local stand_items = {
    "minecraft:compass",
    "minecraft:clock",
    "minecraft:apple",
    "minecraft:arrow"
}

local turtles = {}
turtles["minecraft:compass"] = "compass"
turtles["minecraft:clock"] = "clock"
turtles["minecraft:apple"] = "apple"
turtles["minecraft:arrow"] = "arrow"

local standAgent = stands.StandAgent:new("stand", stand_items, turtles)
local leaderboardAgent = leaderboard.LeaderboardAgent:new("leaderboard")
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(standAgent)
curAgency:registerAgent(leaderboardAgent)

return {start = function() curAgency:run() end}