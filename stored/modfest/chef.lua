local function start()
    local chatter = peripheral.find("chatter")
    local prases = {
        "Mamma-mia",
        "What are you doing?",
        "You should cook meal, not burn them!",
        "Please, for your own sake, stop it!",
        "I wasn't programmed for this!"
    }
    while true do
        chatter.setMessage(prases[math.random(#prases)])
        if math.random() > 0.5 then
            turtle.turnLeft()
        else
            turtle.turnRight()
        end
        os.sleep(5)
    end
end

return {
    start = start
}