-- dependency
-- doerlib

local doerlib = require("doerlib")
local logger = require("core.logging")
local bus = require("core.bus")

local _log = logger.create("smith")

local SmithAgent = doerlib.BaseAgent:new()
SmithAgent.peripheral = {}

function SmithAgent:new(name, loop_delay, o)
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function SmithAgent:configure()
    self.peripheral = peripheral.find("smithingAutomata")
    if self.peripheral == nil then
        error("Cannot find smithing automata")
    end
    self.peripheral.setFuelConsumptionRate(6)
end

function SmithAgent:perform()
    local something, info = turtle.inspect()
    if something then
        if info.name == "minecraft:cobblestone" then
            local smelt, err = self.peripheral.smelt("block")
            if not smelt then
                _log:error("Cannot smelt block", err)
            else
                bus.queueEvent("pusher", "factory1", {chain="smith", ready=true}, true)
            end
        elseif info.name == "minecraft:stone" then
            bus.queueEvent("pusher", "factory1", {chain="smith", ready=true}, true)
        end
    end
end

local smithingAgent = SmithAgent:new(nil, 1)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(smithingAgent)

return {start = function() curAgency:run() end}