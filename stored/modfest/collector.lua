local mode = settings.get("dark_toolkit.collector_mode") or "default"

local function firstEmptySlot()
    for slot = 1, 16 do
        local count = turtle.getItemCount(slot)
        if count == 0 then
            return slot
        end
    end
    return -1
end

local function findChest(chest)
    local chestFound = false
    for _ = 1, 4 do
        local present, blockData = turtle.inspect()
        if present and blockData.name == chest then
            chestFound = true
            break
        end
        turtle.turnLeft()
    end
    if not chestFound then
        return false, "cannot find chest"
    end
    return true
end

local chest_points = {
    "t1", "t2", "t3", "t4", "t5", "t6"
}

local function default_suck()
    local result, err = findChest("extended_drawers:quad_drawer")
    if result then
        local empty_slot = firstEmptySlot()
        if empty_slot ~= -1 then
            local suck_success = turtle.suck(64)
            while suck_success do
                suck_success = turtle.suck(64)
            end
        end
    else
        print(err)
    end
end

local function down_suck()
    local empty_slot = firstEmptySlot()
    if empty_slot ~= -1 then
        local suck_success = turtle.suckDown(64)
        while suck_success do
            suck_success = turtle.suckDown(64)
        end
    end
end

local function start()
    local automata = peripheral.find("endAutomata")
    while true do
        for _, chest_point in ipairs(chest_points) do
            local res, err = automata.warpToPoint(chest_point)
            if not res then
                print(err)
            end
            if mode == "default" then
                default_suck()
            elseif mode == "down" then
                down_suck()
            end
            os.sleep((automata.getCooldown("warp") / 1000) + 1)
        end
        automata.warpToPoint("chest")
        findChest("minecraft:chest")
        for slot=1,16 do
            local item_count = turtle.getItemCount(slot)
            if item_count > 0 then
                turtle.select(slot)
                local result = turtle.drop(64)
                while not result do
                    os.sleep(5)
                    print("Retry dropping")
                    result = turtle.drop(64)
                end
            end            
        end
    end
end

return {
    start = start
}