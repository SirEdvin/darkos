-- dependency
-- doerlib

local doerlib = require("doerlib")
local logger = require("core.logging")
local bus = require("core.bus")

local _log = logger.create("smith")

local BrigadirAgent = doerlib.BaseAgent:new()
BrigadirAgent.peripheral = {}
BrigadirAgent.chatter = {}
BrigadirAgent.voider = {}
BrigadirAgent.file_name = "score.r"
BrigadirAgent.score_table = {amount = 0}
BrigadirAgent.cleaning_counter = 0

function BrigadirAgent:new(name, loop_delay, o)
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function BrigadirAgent:save()
    local opened_file = fs.open(self.file_name, "w")
    opened_file.write(textutils.serialise(self.score_table, {compact=true}))
    opened_file.close()
end

function BrigadirAgent:load()
    local opened_file = fs.open(self.file_name, "r")
    if opened_file ~= nil then
        local raw_data = opened_file.readAll()
        self.score_table = textutils.unserialise(raw_data)
        opened_file.close()
    else
        self.score_table = {amount = 0}
    end
end

function BrigadirAgent:draw()
    self.chatter.setMessage("Already " .. self.score_table.amount .. " blocks produced")
end

function BrigadirAgent:configure()
    self:load()
    self.peripheral = peripheral.find("protectiveAutomata")
    if self.peripheral == nil then
        error("Cannot find protective automata")
    end
    self.peripheral.setFuelConsumptionRate(6)
    self.chatter = peripheral.find("chatter")
    if self.chatter == nil then
        error("Cannot find chatter")
    end
    self.voider = peripheral.find("lava_bucket")
    if self.voider == nil then
        error("Cannot find voider")
    end
    self:draw()
end

function BrigadirAgent:perform()
    local something, info = turtle.inspect()
    if something and info.name == "minecraft:chiseled_stone_bricks" then
        local swing, err = self.peripheral.swing("block")
        if not swing then
            _log:error("Cannot dig block", err)
        else
            bus.queueEvent("pusher", "factory1", {chain="brigadir", ready=true}, true)
            self.score_table.amount = self.score_table.amount + 1
            self:save()
            self:draw()
        end
    elseif not something then
        bus.queueEvent("pusher", "factory1", {chain="brigadir", ready=true}, true)
    end
    self.cleaning_counter = self.cleaning_counter + 1
    if self.cleaning_counter >= 10 then
        for i=2,16 do
            local item_count = turtle.getItemCount(i)
            if item_count > 0 then
                turtle.select(i)
                self.voider.void()
            end
        end
        turtle.select(1)
    end
end

local smithingAgent = BrigadirAgent:new(nil, 1)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(smithingAgent)

return {start = function() curAgency:run() end}