local dfpwm = require("cc.audio.dfpwm")

local token = settings.get("dark_toolkit.alerts_token")


local function is_alert()
    local request = http.get(
        "https://api.alerts.in.ua/v1/alerts/active.json",
        {Authorization = "Bearer " .. token}
    )
    local data = textutils.unserialiseJSON(request.readAll())
    request.close()
    return #data.alerts > 10
end

local function play_siren(speaker)
    local decoder = dfpwm.make_decoder()
    for chunk in io.lines("siren.dfpwm", 16 * 1024) do
        local buffer = decoder(chunk)

        while not speaker.playAudio(buffer, 2) do
            os.pullEvent("speaker_audio_empty")
        end
    end
end

local function warn_about_air_raid_alert(monitor)
    monitor.clear()
    monitor.setTextColor(colors.red)
    monitor.setTextScale(2)
    local x, y = monitor.getSize()
    local first_line = "There is a air raid alarm"
    local second_line = "Please, proceed to shelter!"
    local y_center = (y - 2) / 2 + 1
    monitor.setCursorPos((x - first_line:len()) / 2 + 1, y_center)
    monitor.write(first_line)
    monitor.setCursorPos((x - second_line:len()) / 2 + 1, y_center + 1)
    monitor.write(second_line)
end

local function clear_air_raid_alert(monitor)
    monitor.clear()
    monitor.setTextScale(2)
    monitor.setTextColor(colors.white)
    local x, y = monitor.getSize()
    local first_line = "There is no air raid alarm now"
    local y_center = (y - 1) / 2 + 1
    monitor.setCursorPos((x - first_line:len()) / 2 + 1, y_center)
    monitor.write(first_line)
end

local function start()
    local speaker = peripheral.find("speaker")
    local monitor = peripheral.find("monitor")
    local is_air_raid_alert = false

    while true do
        if not is_air_raid_alert then
            if is_alert() then
                is_air_raid_alert = true
                warn_about_air_raid_alert(monitor)
                play_siren(speaker)
                os.sleep(30)
            else
                clear_air_raid_alert(monitor)
                os.sleep(10)
            end
        else
            if is_alert() then
                is_air_raid_alert = false
                clear_air_raid_alert(monitor)
                os.sleep(30)
            else
                os.sleep(10)
            end
        end
    end
end

return {
    start = start
}