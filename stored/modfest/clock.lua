-- dependency
-- doerlib
-- doerlib.extras:shooter

local doerlib = require("doerlib")
local shooter = require("doerlib.extras.shooter")

local shooterAgent = shooter.ShootAgent:new("shoot", "minecraft:clock")
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(shooterAgent)

return {start = function() curAgency:run() end}