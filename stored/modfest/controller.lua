-- dependency
-- doerlib

local expect = require("cc.expect")

local doerlib = require("doerlib")
local logging = require("core.logging")


local _log = logging.create("controller")

local LoggingAgent = doerlib.BaseAgent:new()

function LoggingAgent:new(name, loop_delay, o)
    expect.expect(1, name, "string")
    expect.expect(2, loop_delay, "number", "nil")
    expect.expect(3, o, "table", "nil")
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    return o
end

function LoggingAgent:configure()
    self._targetEvents = {"logging"}
    self._eventHandlers["logging"] = function(message) _log:info(message) end
end


local loggingAgent = LoggingAgent:new("logging", 0)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(loggingAgent)

return {start = function() curAgency:run() end}