-- dependency
-- doerlib

local doerlib = require("doerlib")
local logger = require("core.logging")
local bus = require("core.bus")

local _log = logger.create("smith")

local PusherAgent = doerlib.BaseAgent:new()
PusherAgent.piston = {}
PusherAgent.pending_push = false
PusherAgent.triggers = {
    mason1 = false,
    mason2 = false,
    smith = false,
    brigadir = false,
}

function PusherAgent:new(name, o)
    o = o or doerlib.BaseAgent:new(name, 0.2, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function PusherAgent:perform()
    if self.pending_push then
        _log:info("Pushing chain")
        local present = turtle.inspect()
        if not present then
            _log:warn("Cannot push because there is not block yet ")
        else
            local r, err = self.piston.push()
            if r then
                for trigger, _ in pairs(self.triggers) do
                    self.triggers[trigger] = false
                end
                self.pending_push = false
            else
                _log:warn("Cannot push because of ", err)
            end
        end
    end
end

function PusherAgent:mark_and_check(trigger)
    if self.triggers[trigger] == nil then
        _log:warn("Cannot find trigger for", trigger)
    elseif not self.triggers[trigger] then
        self.triggers[trigger] = true
        local left_triggers = {}
        for trigger, value in pairs(self.triggers) do
            if not value then
                table.insert(left_triggers, trigger)
            end
        end
        if #left_triggers > 0 then
            _log:info("Waiting for more triggers", table.unpack(left_triggers))
        else
            self.pending_push = true
        end
    end
end

function PusherAgent:configure()
    self.piston = peripheral.find("piston")
    if self.piston == nil then
        error("Cannot find piston")
    end
    self.piston.setSilent(true)
    self._targetEvents = {"factory1"}
    self._eventHandlers = {
        factory1 = function(message)
            if message.ready then
                self:mark_and_check(message.chain)
            end
        end
    }
end

local smithingAgent = PusherAgent:new(nil)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(smithingAgent)

return {start = function() curAgency:run() end}