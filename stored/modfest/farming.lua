-- dependency
-- turtle.brains
-- turtle.brains.gpsbased
-- doerlib
-- doerlib.extras:turtle
-- libs:starNav
-- libs:location
-- libs:aStar
-- libs:tinyMap
-- libs:pQueue

local brains = require("turtle.brains.gpsbased")
local turtle_agent = require("doerlib.extras.turtle")
local doerlib = require("doerlib")

local startX = tonumber(settings.get("dark_toolkit.startX"))
local startY = tonumber(settings.get("dark_toolkit.startY"))
local startZ = tonumber(settings.get("dark_toolkit.startZ"))

local farming_brain = brains.CreativeHoverFarmTurtle:new("extended_drawers:quad_drawer", startX, startZ, startY, 5, 5, 1)
local turtleAgent = turtle_agent.TurtleAgent:new(farming_brain, "farming", 30)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(turtleAgent)

return {start = function() curAgency:run() end}