local function start()
    local chest = peripheral.find("creative_chest")
    local automata = peripheral.find("automata")
    while true do
        chest.generate("minecraft:firework_rocket", 1, "{Fireworks:{Explosions:[{Type:1,Trail:1,Colors:[I;11743532,8073150,14188952,12801229],FadeColors:[I;2437522,8073150]}],Flight:1}}")
        automata.use("block")
        os.sleep(1200)
    end
end

return {
    start = start
}