-- dependency
-- doerlib
-- doerlib.extras
-- turtle.brains
-- turtle.brains.gpsbased

local doerlib = require("doerlib")
local doerlib_extras = require("doerlib.extras")
local turtle_collector = require("turtle.brains.gpsbased.collector")

local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)


local turtleBrain = turtle_collector.LavaCollectorTurtle:new(
    1982, -4346, 21, 6, 6
)
local turtleAgent = doerlib_extras.TurtleAgent:new(turtleBrain)

curAgency:registerAgent(turtleAgent)
curAgency:configure()

return {
    start = function() curAgency:run() end
}
