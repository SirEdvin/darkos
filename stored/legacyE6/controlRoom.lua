local doerlib = require("doerlib")
local doerlib_extras = require("doerlib.extras")
local quartermaster = require("applications.quartermaster")
local builder = require("applications.builder")

-- configuration
local craftableProducts = {
    quartermaster.Product:new("refinedstorage:quartz_enriched_iron"),
    quartermaster.Product:new("minecraft:glass"),
    quartermaster.Product:new("emendatusenigmatica:coke_gem"),
    quartermaster.Product:new("glassential:glass_light", false, 32768),
    quartermaster.Product:new("supplementaries:copper_lantern", false, 512, 512),
    quartermaster.Product:new("byg:glowstone_lamp", false, 2048),
}
local producableProducts = {
    -- technical farm (first part)
    quartermaster.Product:new("minecraft:wheat_seeds"),
    quartermaster.Product:new("minecraft:wheat"),
    quartermaster.Product:new("minecraft:dark_oak_log"),
    quartermaster.Product:new("minecraft:dark_oak_leaves", true, 512),
    quartermaster.Product:new("minecraft:dark_oak_sapling", true, 512),
    quartermaster.Product:new("minecraft:apple"),
    quartermaster.Product:new("minecraft:oak_log", true, 512),
    quartermaster.Product:new("minecraft:oak_leaves", true, 512),
    quartermaster.Product:new("minecraft:oak_sapling"),
    quartermaster.Product:new("minecraft:sugar_cane"),
    quartermaster.Product:new("minecraft:grass"),
    quartermaster.Product:new("minecraft:spruce_log"),
    quartermaster.Product:new("minecraft:spruce_leaves", true, 512),
    quartermaster.Product:new("minecraft:spruce_sapling", true, 512),
    quartermaster.Product:new("minecraft:nether_wart"),
    quartermaster.Product:new("minecraft:carrot"),
    quartermaster.Product:new("minecraft:potato"),
    quartermaster.Product:new("minecraft:poisonous_potato", true, 512, 512),
    quartermaster.Product:new("farmersdelight:onion"),
    quartermaster.Product:new("supplementaries:flax"),
    quartermaster.Product:new("supplementaries:flax_seeds", true, 512),
    quartermaster.Product:new("farmersdelight:tomato"),
    quartermaster.Product:new("farmersdelight:tomato_seeds", true, 512),
    quartermaster.Product:new("simplefarming:sweet_potato"),
    quartermaster.Product:new("simplefarming:sweet_potato_seeds", true, 512),
    quartermaster.Product:new("farmersdelight:rice"),
    quartermaster.Product:new("farmersdelight:rice_panicle", true),
    quartermaster.Product:new("farmersdelight:straw"),
    quartermaster.Product:new("integrateddynamics:menril_sapling", true, 512, 512),
    quartermaster.Product:new("integrateddynamics:menril_log"),
    quartermaster.Product:new("integrateddynamics:menril_leaves", true, 512, 512),
    quartermaster.Product:new("integrateddynamics:menril_berries", true, 512, 512),
    quartermaster.Product:new("minecraft:acacia_log"),
    quartermaster.Product:new("minecraft:acacia_leaves", true, 512),
    quartermaster.Product:new("minecraft:acacia_sapling", true, 512),
    quartermaster.Product:new("alexsmobs:acacia_blossom", true, 512),
    -- technical farm (second part)
    quartermaster.Product:new("minecraft:crimson_fungus"),
    quartermaster.Product:new("minecraft:crimson_stem", true, 1024),
    quartermaster.Product:new("minecraft:nether_wart_block"),
    quartermaster.Product:new("botania:pink_mystical_flower"),
    -- stone factory
    quartermaster.Product:new("masonry:graniteengraved", false, 65536),
    quartermaster.Product:new("byg:black_chiseled_sandstone", false, 65536),
    quartermaster.Product:new("embellishcraft:basalt_large_bricks", false, 65536),
    quartermaster.Product:new("minecraft:sand", false, 65536),
    quartermaster.Product:new("astralsorcery:aquamarine_sand_ore", false, 512, 65536),
}

local dockRequirements = {
    builder.Product:new("embellishcraft:basalt_large_bricks", false, 65536),
    builder.Product:new("masonry:graniteengraved", false, 32768),
    builder.Product:new("glassential:glass_light", false, 32768),
    builder.Product:new("supplementaries:copper_lantern", false, 512),
    builder.Product:new("byg:black_chiseled_sandstone", false, 65536),
    builder.Product:new("byg:glowstone_lamp", false, 1024),
}

local function start()

    local currentAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
    local quartermasterAgent = quartermaster.QuartermasterAgent:new("Quartermaster", peripheral.find("refinedstorage"))
    local providerAgent = builder.MaterialProviderAgent:new(
        "Provider", peripheral.find("refinedstorage"), "dankstorage:dank_tile",
        "up", dockRequirements, 60
    )
    local screenAgent = doerlib_extras.ScreenAgent:new("mainScreen")
    screenAgent:addMonitor(
        peripheral.wrap("monitor_0"),
        quartermaster.QuartermasterPanel:new(quartermasterAgent),
        quartermasterAgent.name
    )

    quartermasterAgent:registerProducts(producableProducts)
    quartermasterAgent:registerProducts(craftableProducts)

    currentAgency:configure()
    currentAgency:registerAgent(screenAgent)
    currentAgency:registerAgent(quartermasterAgent)
    currentAgency:registerAgent(providerAgent)
    currentAgency:run()
end

return {
    start = start
}
