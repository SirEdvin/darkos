local pretty = require "cc.pretty"

local function start()
    while true do
        local eventData = {os.pullEvent("enderwire_computer_event")}
        pretty.print(pretty.pretty(eventData))
    end
end

return {
    start = start
}