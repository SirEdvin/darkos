local pretty = require "cc.pretty"

local function start()
    while true do
        sleep(0.05)
        commands.exec("weather clear")
        commands.exec("time set day")
    
    end
end

return {
    start = start
}