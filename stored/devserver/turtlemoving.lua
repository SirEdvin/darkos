local function start()
    local p = peripheral.wrap("right")
    while true do
        p.printToConsole("I am still alive, fuel level " .. turtle.getFuelLevel())
        turtle.forward()
        sleep(1)
    end
end

return {
    start = start
}