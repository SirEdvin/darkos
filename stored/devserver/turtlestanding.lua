local function start()
    local p = peripheral.wrap("right")
    while true do
        p.printToConsole("I am still alive")
        sleep(10)
    end
end

return {
    start = start
}