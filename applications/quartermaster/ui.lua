local expect = require("cc.expect")
local bus = require("core.bus")
local utils = require("core.utils")

local ui = require("ui")
local quartermaster_agent = require("applications.quartermaster.agent")
local quartermaster_model = require("applications.quartermaster.model")

local SelectableButton = ui.PseudoButton:new("fake")
SelectableButton.selectedColor = colors.gray
SelectableButton.selected = false

function SelectableButton:new(text, baseColor, disabledColor, selectedColor, o)
    expect.expect(1, text, "string")
    expect.expect(2, baseColor, "number", "nil")
    expect.expect(3, disabledColor, "number", "nil")
    expect.expect(4, selectedColor, "number", "nil")
    expect.expect(5, o, "table", "nil")

    o = o or ui.PseudoButton:new(text, baseColor, disabledColor, o)
    setmetatable(o, self)
    self.__index = self

    o.selectedColor = selectedColor or o.selectedColor
    o.selected = false

    return o
end

function SelectableButton:select()
    self.selected = true
end

function SelectableButton:unselect()
    self.selected = false
end

function SelectableButton:_draw(context)
    local curBackgroundColor = context.monitor.getBackgroundColor()
    if self.selected then
        context.monitor.setBackgroundColor(self.selectedColor)
    end
    ui.PseudoButton._draw(self, context)
    context.monitor.setBackgroundColor(curBackgroundColor)
end

local SelectionPanel = ui.UIContainer:new()
SelectableButton.selectedOption = ""

function SelectionPanel:new(options, selected, o)
    expect.expect(1, options, "table")
    expect.expect(2, selected, "string")

    o = o or ui.UIContainer:new(o)
    setmetatable(o, self)
    self.__index = self

    for code, text in pairs(options) do
        o.childrens[code] = SelectableButton:new(text)
        if code == selected then
            o.childrens[code]:select()
            o.selectedOption = code
        end
    end
    o:onClick(function(source, x, y)
        for code, el in pairs(source.childrens) do
            if el:isInside(x, y) then
                el.selected = true
                source.childrens[source.selectedOption].selected = false
                source.selectedOption = code
                bus.queueLocalEvent(bus.EventType.DATA_CHANGED, {monitor = source.monitorName})
            end
        end
    end)
    return o
end

function SelectionPanel:updateText(calculatedCount)
    for state, count in pairs(calculatedCount) do
        self.childrens[state].text = utils.title(state) .. " (" .. tostring(count) .. ")"
    end
end

function SelectionPanel:_draw(context)
    for _, el in pairs(self.childrens) do
        el:draw(context)
    end
end

local QuartermasterPanel = ui.UIContainer:new()
QuartermasterPanel.agent = quartermaster_agent.QuartermasterAgent:new("fake", {})

function QuartermasterPanel:new(agent, o)
    o = o or ui.UIContainer:new(o)
    setmetatable(o, self)
    self.__index = self
    o.agent = agent
    local selections = {}
    selections[quartermaster_model.ProductState.REQUIRED] = "Required"
    selections[quartermaster_model.ProductState.FULFILLED] = "Fulfilled"
    selections[quartermaster_model.ProductState.EXCESSIVE] = "Excessive"
    o.childrens.selection = SelectionPanel:new(selections, quartermaster_model.ProductState.REQUIRED)
    o.childrens.divider = ui.HorizontalDivider:new()
    return o
end

function QuartermasterPanel:_draw(context)
    local curX, _ = context.monitor.getCursorPos()
    self.childrens.selection:updateText(self.agent.registry:calculateCountByState())
    context:slideDownAfter(self.childrens.selection.draw, self.childrens.selection, context)
    self.childrens.divider:draw(context)
    local _, curY = context.monitor.getCursorPos()
    for _, productName in pairs(self.agent.registry.productNames) do
        local product = self.agent.registry.products[productName]
        if not product.passive or not context.collapsed then
            local productState = product:getState()
            if productState == self.childrens.selection.selectedOption then
                curY = curY + 1
                context.monitor.setCursorPos(curX, curY)
                context.monitor.write(product.displayName .. ": ")
                local selectedColor
                local selectedText
                if productState == quartermaster_model.ProductState.REQUIRED then
                    selectedColor = colors.green
                    selectedText = tostring(product.current_amount) .. "/" .. tostring(product.target_amount)
                elseif productState == quartermaster_model.ProductState.EXCESSIVE then
                    selectedColor = colors.red
                    selectedText = tostring(product.current_amount) .. "/" .. tostring(product.excessive_amount)
                else
                    selectedColor = colors.yellow
                    selectedText = tostring(product.current_amount) .. "/" .. tostring(product.excessive_amount)
                end
                context:withColor(selectedColor, context.monitor.write, selectedText)
            end
        end
    end
end

return {
    QuartermasterPanel = QuartermasterPanel,
}