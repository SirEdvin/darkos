local quartermaster = require("applications.quartermaster.agent.quartermaster")
local taskmaster = require("applications.quartermaster.agent.taskmaster")
local sorter = require("applications.quartermaster.agent.sorter")

return {
    QuartermasterAgent = quartermaster.QuartermasterAgent,
    TaskmasterAgent = taskmaster.TaskmasterAgent,
    SorterAgent = sorter.SorterAgent,
}