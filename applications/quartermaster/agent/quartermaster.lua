local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local quartermaster_model = require("applications.quartermaster.model")

local _log = logging.create("agents.rs")

local QuartermasterAgent = doerlib.BaseAgent:new()
QuartermasterAgent.rsPeripheral = nil
QuartermasterAgent.registry = quartermaster_model.ProductRegistry:new()
QuartermasterAgent.updateTimestamp = nil

function QuartermasterAgent:new(name, rsPeripheral, loop_delay, o)
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.rsPeripheral = rsPeripheral
    o.registry = quartermaster_model.ProductRegistry:new()
    return o
end

function QuartermasterAgent:configure()
end

function QuartermasterAgent:registerProduct(product)
    self.registry:add(product)
end

function QuartermasterAgent:registerProducts(products)
    for _, product in pairs(products) do
        self:registerProduct(product)
    end
end

function QuartermasterAgent:updateProductData()
    local ran, items = pcall(self.rsPeripheral.getItems)
    if ran and items then
        self.registry:update(items)
        self.updateTimestamp = os.time()
    else
        _log:error("Problem with access RS peripheral, waiting ...")
    end
    bus.queueLocalEvent(bus.EventType.DATA_CHANGED, {source = self.name})
end

function QuartermasterAgent:tryCraftProduct(product, craftTasks)
    local item = {name = product.name}
    if not self.rsPeripheral.hasPattern(item) then
        return false
    end
    -- search if craft tasks exists
    local needAmount = product:getNeedAmount()
    for _, craftTask in pairs(craftTasks) do
        if craftTask.stack.item and craftTask.stack.item.name == product.name then
            needAmount = needAmount - craftTask.stack.item.count
        end
    end
    if needAmount <= 0 then
        return true
    end
    local result = self.rsPeripheral.scheduleTask(item, needAmount)
    if result == nil then
        _log:warn("Crafting for", product.name, "cannot be scheduled")
        return false
    end
    return true
end

function QuartermasterAgent:processDemand()
    local craftTasks = self.rsPeripheral.getTasks()
    local requiredProducts = {}
    local excessiveProducts = {}
    for _, product in pairs(self.registry.products) do
        local productState = product:getState()
        if productState == quartermaster_model.ProductState.REQUIRED then
            local scheduleResult = self:tryCraftProduct(product, craftTasks)
            if not scheduleResult then
                requiredProducts[product.name] = true
            end
        elseif productState == quartermaster_model.ProductState.EXCESSIVE then
            excessiveProducts[product.name] = true
        end
    end
    bus.broadcastEvent(
        bus.EventType.FABRICATION_INFORMATION,
        {required = requiredProducts, excessive = excessiveProducts}
    )
end

function QuartermasterAgent:perform()
    self:updateProductData()
    self:processDemand()
end

function QuartermasterAgent:cleanup()
    doerlib.BaseAgent.cleanup(self)
    bus.broadcastEvent(
        bus.EventType.FABRICATION_INFORMATION,
        {required = {}, excessive = {}}
    )
end

return {
    QuartermasterAgent = QuartermasterAgent,
}