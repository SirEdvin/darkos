local expect = require("cc.expect")

local _DEFAULT_TARGET_AMOUNT = 6144
local _DEFAULT_EXCESSIVE_AMOUNT = 24576


local ProductState = {
    REQUIRED = "REQUIRED",
    FULFILLED = "FULFILLED",
    EXCESSIVE = "EXCESSIVE",
    EPHEMERAL = "EPHEMERAL"
}
local Product = {
    name = "", displayName = "", target_amount = _DEFAULT_TARGET_AMOUNT, ephemeral = false,
    excessive_amount = _DEFAULT_EXCESSIVE_AMOUNT, current_amount = 0, passive = false
}

function Product:new(name, passive, target_amount, excessive_amount, o)
    expect.expect(1, name, "string")
    expect.expect(2, passive, "boolean", "nil")
    expect.expect(3, target_amount, "number", "nil")
    expect.expect(3, excessive_amount, "number", "nil")
    expect.expect(5, o, "table", "nil")

    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.passive = passive or false
    o.target_amount = target_amount or _DEFAULT_TARGET_AMOUNT
    o.excessive_amount = excessive_amount or math.max(_DEFAULT_EXCESSIVE_AMOUNT, o.target_amount + 1)
    return o
end

function Product:getNeedAmount()
    if self.target_amount <= self.current_amount then
        return 0
    end
    return self.target_amount - self.current_amount
end

function Product:getState()
    if self.ephemeral then
        return ProductState.EPHEMERAL
    end
    if self.current_amount > self.excessive_amount then
        return ProductState.EXCESSIVE
    end
    if not self.passive and (self.target_amount > self.current_amount) then
        return ProductState.REQUIRED
    end
    return ProductState.FULFILLED
end

function Product:updateInformation(itemData)
    if itemData.displayName then
        self.displayName = itemData.displayName
    end
    self.current_amount = self.current_amount + itemData.count
end

function Product:startInformationUpdate()
    self.current_amount = 0
    self.ephemeral = true
end

function Product:finishInformationUpdate()
    self.ephemeral = false
end

local ProductRegistry = {
    products = {fake = Product:new("fake")},
    productNames = {"fake"}
}

function ProductRegistry:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.products = {}
    o.productNames = {}
    return o
end

function ProductRegistry:add(product)
    if not self.products[product.name] then
        self.products[product.name] = product
        table.insert(self.productNames, product.name)
        table.sort(self.productNames)
    else
        error("Cannot duplicate products!")
    end
end

function ProductRegistry:update(storageData)
    for _, product in pairs(self.products) do
        product:startInformationUpdate()
    end
    for _, itemData in pairs(storageData) do
        if self.products[itemData.name] then
            self.products[itemData.name]:updateInformation(itemData)
        end
    end
    for _, product in pairs(self.products) do
        product:finishInformationUpdate()
    end
end

function ProductRegistry:calculateDemand()
    local demand = {}
    for _, product in pairs(self.products) do
        local productNeedAmount = product:getNeedAmount()
        if productNeedAmount > 0 then
            demand[product.name] = productNeedAmount
        end
    end
    return demand
end

function ProductRegistry:calculateCountByState()
    local countByState = {}
    for _, product in pairs(self.products) do
        local produtState = product:getState()
        if countByState[produtState] == nil then
            countByState[produtState] = 0
        end
        countByState[produtState] = countByState[produtState] + 1
    end
    return countByState
end

return {
    ProductState = ProductState,
    Product = Product,
    ProductRegistry = ProductRegistry,
}
