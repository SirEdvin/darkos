local doerlib = require("doerlib")
local ui = require("ui")
local supervisor_agent = require("applications.supervisor.agent")
local supervisor_model = require("applications.supervisor.model")

local AgencyPanel = ui.UIContainer:new()
AgencyPanel.data = supervisor_model.AgencyData:new(
    {
        name = "fake", state = "fale", type = "fake",
        agents = {}, build = "fake", version = "fake",
        networkName = "fake"
    }
)

function AgencyPanel:new(data, o)
    o = o or ui.UIContainer:new(o)
    setmetatable(o, self)
    self.__index = self
    o.data = data
    o.childrens.rebootButton = ui.PseudoButton:new("Reboot")
    o.childrens.updateButton = ui.PseudoButton:new("Update")
    o.childrens.rebootButton:onClick(function (_, _, _)
        doerlib.AgencyController.rebootAgency(o.data.networkName)
    end)
    o.childrens.updateButton:onClick(function (_, _, _)
        doerlib.AgencyController.updateAgency(o.data.networkName)
    end)
    return o
end

function AgencyPanel:_draw(context)
    context:slideDownAfter(context.monitor.write, self.data.name .. " (build " .. self.data.build .. ")")
    local curX, curY = context.monitor.getCursorPos()
    context.monitor.write((string.lower(self.data.state or "UNKNOWN")) .. " ")
    if self.data.state == doerlib.AgencyState.WORKING then
        self.childrens.updateButton:draw(context)
        self.childrens.rebootButton:draw(context)
    end
    curX = curX + 4
    for agentName, agentState in pairs(self.data.agents) do
        curY = curY + 1
        context.monitor.setCursorPos(curX, curY)
        context.monitor.write(agentName .. ": " .. string.lower(agentState))
    end
end

local AgencyListPanel = ui.UIContainer:new()
AgencyListPanel.data = supervisor_model.AgencyList:new()

function AgencyListPanel:new(agencyList, o)
    o = o or ui.UIContainer:new(o)
    setmetatable(o, self)
    self.__index = self
    o.data = agencyList
    return o
end

function AgencyListPanel:_updateUIComponents()
    for _, name in pairs(self.data.agencyNames) do
        if self.childrens[name .. "panel"] == nil then
            self.childrens[name .. "panel"] = AgencyPanel:new(self.data.agencies[name])
            self.childrens[name .. "divider"] = ui.HorizontalDivider:new()
        end
    end
end

function AgencyListPanel:_draw(context)
    self:_updateUIComponents()
    for _, name in pairs(self.data.agencyNames) do
        if not context.collapsed or self.data.agencies[name].type ~= doerlib.AgencyType.SLAVE then
            local divider = self.childrens[name .. "divider"]
            local panel = self.childrens[name .. "panel"]
            context:slideDownAfter(divider.draw, divider, context)
            context:slideDownAfter(panel.draw, panel, context)
        end
    end
end

local SupervisorPanel = ui.UIContainer:new()
SupervisorPanel.agent = supervisor_agent.AgencySupervisorAgent:new("fake")

function SupervisorPanel:new(agent, o)
    o = o or ui.UIContainer:new(o)
    setmetatable(o, self)
    self.__index = self
    o.agent = agent
    o.childrens.updateAll = ui.PseudoButton:new("Update All")
    o.childrens.updateAll:onClick(function(_, _, _)
        doerlib.AgencyController.updateAll()
    end)
    o.childrens.listPanel = AgencyListPanel:new(agent.data)
    return o
end

function SupervisorPanel:_draw(context)
    context:slideDownAfter(self.childrens.updateAll.draw, self.childrens.updateAll, context)
    self.childrens.listPanel:draw(context)
end

return {
    SupervisorPanel = SupervisorPanel,
}