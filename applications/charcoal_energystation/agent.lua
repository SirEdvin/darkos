local expect = require("cc.expect")

local doerlib = require("doerlib")
local logging = require("core.logging")

local _log = logging.create("application.burner.agent")

local _WOOD = "minecraft:oak_log"
local _COAL = "minecraft:charcoal"

local BurnerAgent = doerlib.BaseAgent:new()
BurnerAgent.woodStorageName = ""
BurnerAgent.furnaceName = ""
BurnerAgent.storageName = ""
BurnerAgent.woodStorages = {}
BurnerAgent.furnaces = {}
BurnerAgent.storages = {}

local function isInventoryFullWithItems(inventory, item)
    for slot = 1, inventory.size() do
        local detail = inventory.getItemDetail(slot)
        if detail == nil then
            return false
        end
        if detail.name == item and detail.count ~= detail.maxCount then
            return false
        end
    end
    return true
end

function BurnerAgent:new(name, woodStorageName, furnaceName, storageName, loop_delay, o)
    expect.expect(1, name, "string")
    expect.expect(2, woodStorageName, "string")
    expect.expect(3, furnaceName, "string")
    expect.expect(4, storageName, "string")
    expect.expect(5, loop_delay, "number", "nil")
    expect.expect(6, o, "table", "nil")
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.woodStorageName = woodStorageName
    o.furnaceName = furnaceName
    o.storageName = storageName
    return o
end

function BurnerAgent:isStoragesAreFull()
    for _, storage in pairs(self.storages) do
        if not isInventoryFullWithItems(storage, _COAL) then
            return false
        end
    end
    return true
end

function BurnerAgent:findStorage()
    for _, storage in pairs(self.storages) do
        if not isInventoryFullWithItems(storage, _COAL) then
            return storage
        end
    end
    return nil
end

function BurnerAgent:findFurnace()
    for _, furnace in pairs(self.furnaces) do
        local detail = furnace.getItemDetail(1)
        if detail == nil then
            return furnace
        end
        if detail.name == _WOOD and detail.count ~= detail.maxCount then
            return furnace
        end
    end
    return nil
end

function BurnerAgent:findWood()
    for _, woodStorage in pairs(self.woodStorages) do
        for slot = 1, woodStorage.size() do
            local detail = woodStorage.getItemDetail(slot)
            if detail ~= nil and detail.name == _WOOD then
                return woodStorage, slot
            end
        end
    end
    return nil, nil
end

function BurnerAgent:freeFurnaces()
    for _, furnace in pairs(self.furnaces) do
        local detail = furnace.getItemDetail(2)
        if detail ~= nil then
            repeat
                local storage = self:findStorage()
                if storage == nil then
                    return false
                end
                furnace.pushItems(peripheral.getName(storage), 2)
                detail = furnace.getItemDetail(2)
            until detail == nil
        end
    end
    return true
end

function BurnerAgent:perform()
    if self:isStoragesAreFull() then
        _log:info("Storage are full, skiping fuel burning")
        return
    end
    if not self:freeFurnaces() then
        _log:info("Cannot find free furnace, skip fuel burning")
        return
    end
    local woodStorage, slot = self:findWood()
    if woodStorage == nil then
        _log:info("Cannot find wood in system, skip fuel burning")
        return
    end
    local furnace = self:findFurnace()
    if furnace == nil then
        _log:info("Cannot find free furnace, skip fuel burning")
        return
    end
    woodStorage.pushItems(peripheral.getName(furnace), slot)
end

function BurnerAgent:configure()
    -- Detect all peripherals storages
    self.woodStorages = {peripheral.find(self.woodStorageName)}
    self.furnaces = {peripheral.find(self.furnaceName)}
    self.storages = {peripheral.find(self.storageName)}
end

local FuelAgent = doerlib.BaseAgent:new()
FuelAgent.inputName = ""
FuelAgent.storageName = ""
FuelAgent.enegryStorageName = ""
FuelAgent.burnRate = 96000
FuelAgent.storages = {}
FuelAgent.input = {}
FuelAgent.energyStorage = {}

function FuelAgent:new(name, inputName, storageName, energyStorageName, burnRate, loop_delay, o)
    expect.expect(1, name, "string")
    expect.expect(2, inputName, "string")
    expect.expect(3, storageName, "string")
    expect.expect(4, energyStorageName, "string")
    expect.expect(5, burnRate, "number", "nil")
    expect.expect(6, loop_delay, "number", "nil")
    expect.expect(7, o, "table", "nil")
    o = o or doerlib.BaseAgent:new(name, loop_delay or 30, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.inputName = inputName
    o.storageName = storageName
    o.energyStorageName = energyStorageName
    o.burnRate = burnRate or o.burnRate
    return o
end

function FuelAgent:findCoal()
    for _, storage in pairs(self.storages) do
        for slot = 1, storage.size() do
            local detail = storage.getItemDetail(slot)
            if detail ~= nil and detail.name == _COAL then
                return storage, slot
            end
        end
    end
    return nil, nil
end

function FuelAgent:configure()
    self.input = peripheral.find(self.inputName)
    self.storages = {peripheral.find(self.storageName)}
    self.energyStorage = peripheral.find(self.energyStorageName)
end

function FuelAgent:perform()
    local energyDiff = self.energyStorage.getEnergyCapacity() - self.energyStorage.getEnergy()
    if energyDiff < self.burnRate then
        _log:info("Energy storage is full")
        return
    end
    if isInventoryFullWithItems(self.input, _COAL) then
       _log:info("Input is full with coal")
       return
    end
    local storage, slot = self:findCoal()
    if storage == nil then
        _log:info("Cannot find coal")
        return
    end
    storage.pushItems(peripheral.getName(self.input), slot, 1)
end

return {
    BurnerAgent = BurnerAgent,
    FuelAgent = FuelAgent,
}