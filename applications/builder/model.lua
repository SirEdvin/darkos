local shared_product = require("applications.shared.product")

return {
    Product = shared_product.Product,
    ProductRegistry = shared_product.ProductRegistry,
    ProductState = shared_product.ProductState,
}