local agent = require("applications.builder.agent")
local model = require("applications.builder.model")

return {
    MaterialProviderAgent = agent.MaterialProviderAgent,
    Product = model.Product,
}