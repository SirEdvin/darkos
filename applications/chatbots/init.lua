local agent = require("applications.chatbots.agent")

return {
    ChatGuideAgent = agent.ChatGuideAgent
}
