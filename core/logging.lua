--[[
    Logger module for ComputerCraft by AlexDevs
    (c) 2021 AlexDevs
    Usage:
        -- Load module
        local Logger = require("logger")
        -- Create new Logger instance
        local logger = Logger(name: string, options: table): logger table
        options:
            outputPath: string = nil -- File path to save logs
            dateFormat: string = "%X" -- Date format for os.date
            format: string = "[%date%] <%name%> [%level%] %log%" -- Template for logs
            colored: boolean = true -- Use colored outputs. Uses term.setTextColor
            stdout: function = print -- Function to use to print logs
            level: number = 20 -- logging level
            colors:
                debug: number = colors.gray -- Color for debug logs
                info: number = colors.white -- Color for info logs
                warn: number = colors.yellow -- Color for warn logs
                error: number = colors.red -- Color for error logs
                default: number = info -- Color for custom logs
        Methods:
            logger:debug(...): boolean -- Log debug, returns true if debug logging is enabled, false if otherwise
            logger:info(...): void -- Log information
            logger:warn(...): void -- Log warnings
            logger:error(...): void -- Log errors
            logger:log(level: string, ...): void -- Log with custom level
            logger:open(): void -- Open file handle of output path (open by default is outputPath is used)
            logger:close(): void -- Close file handle of output path (run this after you finished using the logger)
]]

local utils = require("core.utils")

---@class Logger
---@field name string
---@field dateFormat string
---@field format string
---@field stdout function
---@field level number
---@field colors table
local Logger = {}

local LEVELS = {
    DEBUG = 0,
    INFO = 10,
    SUCCESS = 15,
    WARN = 20,
    ERROR = 30
}

-- Logger constructor
---@type Logger
---@param name string
---@param o any
---@return Logger
local function new(name, o)
    o = o or {}
    local options = {}
    options.name = name or ""
    options.dateFormat = o.dateFormat or "%X"
    options.format = o.format or "[%date%] <%name%> [%level%] %log%"

    options.stdout = o.stdout or print
    options.level = o.level or LEVELS.INFO

    o.colors = o.colors or {}
    options.colors = {}
    options.colors.debug = o.colors.debug or colors.gray
    options.colors.info = o.colors.info or colors.white
    options.colors.warn = o.colors.warn or colors.yellow
    options.colors.error = o.colors.error or colors.red
    options.colors.default = o.colors.default or options.colors.info
    options.colors.success = o.colors.success or colors.green

    local logger = setmetatable(options, {__index = Logger})

    return logger
end

local function formatDate(format)
    return os.date(format)
end

local function serialize(...)
    local out = {}
    local args = table.pack(...)
    for i = 1, args.n do
        table.insert(out, tostring(args[i]))
    end

    return table.concat(out, " ")
end


function Logger:formatTemplate(template, date, name, level, log)
    return template
    :gsub("%%date%%", formatDate(date))
    :gsub("%%name%%", tostring(name))
    :gsub("%%level%%", tostring(level))
    :gsub("%%log%%", tostring(log))
end

-- Debug log
function Logger:debug(...)
    self:log("DEBUG", ...)
end

-- Log info
function Logger:info(...)
    self:log("INFO", ...)
end

-- Log warnings
function Logger:warn(...)
    self:log("WARN", ...)
end

function Logger:success(...)
    self:log("SUCCESS", ...)
end

-- Log errors
function Logger:error(...)
    self:log("ERROR", ...)
end

-- Log info with custom level
---@param label string
function Logger:log(label, ...)
    local logLevel = tostring(label)
    if (LEVELS[logLevel] < self.level) then
        return
    end
    local output = self:formatTemplate(self.format, self.dateFormat, tostring(self.name), tostring(label), serialize(...))
    utils.withTermColor(self.colors.default, self.stdout, output)
end

return {
    create = new,
    LEVELS = LEVELS
}