local logging = require("core.logging")

local _PROTOCOL_NAME = "ScaryBus"
local _PROTOCOL_VERSION = "v1"
local _PROTOCOL = _PROTOCOL_NAME .. _PROTOCOL_VERSION
local _PROTOCOL_TIMEOUT = 20

local EventType = {
    FABRICATION_INFORMATION = "FABRICATION_INFORMATION",
    REDSTONE_CONTROL_MESSAGE = "REDSTONE_CONTROL_MESSAGE",
    DATA_CHANGED = "DATA_CHANGED",
    AGENCY_INFORMATION_MESSAGE = "AGENCY_INFORMATION_MESSAGE",
    AGENCY_CONTROL_MESSAGE = "AGENCY_CONTROL_MESSAGE",
    SPAWNER_INFO_MESSAGE = "SPAWNER_INFO_MESSAGE",
}

local LOCAL_DESTINATION = "__local__"
local BROADCAST_DESTINATION = "__broadcast__"

local _log = logging.create("bus")

local EventBus = {name = "Unknown", enabled = false, networkTargetEvents = {}}

function EventBus:new(name, debug, o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    self.name = name or os.getComputerLabel()
    self.debug = debug or false
    return o
end

function EventBus:listenEvent(event)
    self.networkTargetEvents[event] = true
end

function EventBus:unlistenEvent(event)
    self.networkTargetEvents[event] = false
end

function EventBus:queueEvent(destination, type, message, silent)
    if destination == LOCAL_DESTINATION then
        self:queueLocalEvent(type, message)
    elseif destination == BROADCAST_DESTINATION then
        self:broadcastEvent(type, message)
    else
        local locationID = rednet.lookup(_PROTOCOL, destination)
        if locationID then
            rednet.send(locationID, {type = type, message = message}, _PROTOCOL)
        else
            if not silent then
                _log:warn("Cannot find location " .. destination)
            end
        end
    end
end

function EventBus:queueLocalEvent(type, message)
    os.queueEvent(type, message)
end

function EventBus:broadcastEvent(type, message)
    if self.debug then
        _log:info("Send message " .. type)
    end
    if self.networkTargetEvents[type] then
        self:queueLocalEvent(type, message)
    end
    rednet.broadcast({type = type, message = message}, _PROTOCOL)
end

function EventBus:processMessage(message)
    if self.debug then
        _log:info("Received message " .. message.type)
    end
    if self.networkTargetEvents[message.type] then
        if self.debug then
            _log:info("Queue this message")
        end
        os.queueEvent(message.type, message.message)
    end
end

function EventBus:start()
    _G.BUS = self
    local modem = peripheral.find("modem", function (_, modem) return modem.isWireless() end)
    if modem == nil then
        _log:info("No modem find, skip remote processing")
        return
    end
    local modem_name = peripheral.getName(modem)
    if not rednet.isOpen(modem_name) then
        rednet.open(modem_name)
    end
    rednet.host(_PROTOCOL, self.name)
    _log:info("Host protocol as " .. self.name)
    self.enabled = true
    while self.enabled do
        local senderID, message = rednet.receive(_PROTOCOL, _PROTOCOL_TIMEOUT)
        if senderID then
            self:processMessage(message)
        end
    end
end

function EventBus:stop()
    self.enabled = false
end

local function queueEvent(destination, type, message, silent)
    BUS:queueEvent(destination, type, message, silent)
end

local function queueLocalEvent(type, message)
    BUS:queueLocalEvent(type, message)
end

local function broadcastEvent(type, message)
    BUS:broadcastEvent(type, message)
end

local function shutdownBus()
    if _G.BUS ~= nil then
        BUS:stop()
    end
end

return {
    LOCAL_DESTINATION = LOCAL_DESTINATION,
    PROTOCOL = _PROTOCOL,
    EventType = EventType,
    EventBus = EventBus,
    queueLocalEvent = queueLocalEvent,
    queueEvent = queueEvent,
    broadcastEvent = broadcastEvent,
    shutdownBus = shutdownBus,
}