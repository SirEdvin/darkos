local function firstEmptySlot()
    for slot = 1, 16 do
        local count = turtle.getItemCount(slot)
        if count == 0 then
            return slot
        end
    end
    return -1
end

local function withSlot(slot, func, ...)
    local prevSlot = turtle.getSelectedSlot()
    turtle.select(slot)
    local res = func(...)
    turtle.select(prevSlot)
    return res
end

return {
    firstEmptySlot = firstEmptySlot,
    withSlot = withSlot
}
