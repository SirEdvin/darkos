-- dependency
-- libs:png
-- libs:statue
-- libs:player_skin
-- libs:base64

package.path = package.path .. ";/dark_toolkit/?.lua;/dark_toolkit/?/init.lua"

local selectedAction = select(1, ...)
local passed = { select(2, ...) }

local statue = require("libs.statue")
local player_skin = require("libs.player_skin")
local pretty = require("cc.pretty")

local subcommands = {
    head = {
        usage = "dprint head [playerName]",
		flags = {
		}
    }
}

local action = subcommands[selectedAction]

if not action then
	for _, v in pairs(subcommands) do
		print(v.usage)
		print(" ")
	end
	return
end

-- get flag arguments
local function findFlag(arg)
	if arg.sub(1,1) == '-' then
		if not action.flags[arg] then
			return printError(('Unknown flag argument %s'):format(arg))
		end
	end
	return action.flags[arg]
end

local args = {}
local flags = {}
local lastFlagged = 0
for k, arg in pairs(passed) do
	if k ~= lastFlagged then
		local flagName = findFlag(arg)
		if flagName then
			flags[flagName] = passed[k + 1] or true
			lastFlagged = k + 1 or true
		else
			table.insert(args, arg)
		end
	end
end

if action == subcommands.head then
    local player_name = args[1]
    local result, value = player_skin.getSkinByName(player_name)
    if not result then
        print(value)
    else
        local cubes = statue.wrapPlayerHead(value)
        local p = peripheral.find('statue_workbench')
		if p ~= nil then
			if not p.isPresent() then
				print("Statue is not present!")
			else
				p.setCubes(cubes)
			end
		else
			print("Cannot find statue workbench")
			pretty.pretty_print(cubes)
		end
    end
end
