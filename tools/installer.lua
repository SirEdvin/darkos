local response = http.get("https://gitlab.com/SirEdvin/darkos/-/raw/master/updater.lua")

fs.makeDir("dark_toolkit")

local f = fs.open("dark_toolkit/updater.lua", "w")
f.write(response.readAll())
response.close()
f.close()

local updater = require("/dark_toolkit/updater")
updater.updateDarkToolkit(true)
