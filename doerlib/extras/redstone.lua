local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local _log = logging.create("doerlib.extras.redstone")

local RedstoneAgent = doerlib.BaseAgent:new()
RedstoneAgent.sides = {
    bottom = false,
    top = false,
    left = true,
    right = true,
    back = true,
    front = true
}


function RedstoneAgent:new(name, o)
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function RedstoneAgent:configure()
    self._targetEvents = {bus.EventType.REDSTONE_CONTROL_MESSAGE}
    self._eventHandlers = {}
    self._eventHandlers[bus.EventType.REDSTONE_CONTROL_MESSAGE] = function(redstoneConfig)
        self:processRedstoneConfig(redstoneConfig)
    end
end


function RedstoneAgent:disableSide(side)
    self.sides[side] = false
end

function RedstoneAgent:enableSide(side)
    self.sides[side] = true
end

function RedstoneAgent:processRedstoneConfig(redstoneConfig)
    for side, power in pairs(redstoneConfig) do
        if self.sides[side] then
            _log:info("Process message, update " .. side .. " with power " .. power)
            redstone.setAnalogOutput(side, power)
        end
    end
end

function RedstoneAgent:cleanup()
    doerlib.BaseAgent.cleanup(self)
    for side, _ in pairs(self.sides) do
        redstone.setOutput(side, false)
    end
end

return {
    RedstoneAgent = RedstoneAgent
}
