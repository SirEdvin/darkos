-- Originall developed by ralphgod3, code from here https://pastebin.com/cujThihr

--amount of recipes to check for each crafting handler if it has this many
local recipesToCheck = 100

local logging = require("core.logging")
local recipeCompact = require("libs.recipeRegistryCompact")
local pretty = require("cc.pretty")

local IGNORE_LIST = {
    ["immersiveengineering:bottling_machine"] = true,
    ["mekanism:smelting"] = true,
    ["thermal:brewer"] = true,
    ["woot:factory"] = true,
}

recipeCompact.updateIgnoreList(IGNORE_LIST)

local verifyRetCodes = {
    ["ok"] = 0,
    ["noItem"] = 1,
    ["isAir"] = 2,
    ["noEntries"] = 3,
    ["corrupter"] = 4,
    ["emptyTable"] = 5,
    ["nilItem"] = 6,
}

---gets table length for tables that may not be indexed by number
---@param tableToGetLengthOn table
local function getTableLength(tableToGetLengthOn)
    local len = 0
    for _,_ in pairs(tableToGetLengthOn) do
        len = len + 1
    end
    return len
end

local function correctRecord(value)
    if value == nil then
        return verifyRetCodes.corrupter
    end
    if type(value) == "table" and (getTableLength(value)) == 0 then
        return verifyRetCodes.emptyTable
    end
    return verifyRetCodes.ok
end

local function notAir(value)
    if value == "minecraft:air" then
        return verifyRetCodes.isAir
    end
    return verifyRetCodes.ok
end


local fieldsCheckers = {
    ["name"] = {correctRecord, notAir},
    ["id"] = {correctRecord, notAir},
    ["item"] = {correctRecord, notAir},
    ["tag"] = {correctRecord, notAir},
    ["FluidName"] = {correctRecord},
    ["fluid"] = {correctRecord},
    ["gasName"] = {correctRecord},
    ["slurryName"] = {correctRecord},
    ["entity"] = {correctRecord},
    ["energy"] = {correctRecord},
    ["infuseTypeName"] = {correctRecord},
    ["reagent"] = {correctRecord},
    ["enchantment"] = {correctRecord}
}


---------------------------------------- LOCAL FUNCTIONS ------------------------------------
-- create a pool of recipe registries for faster testing
local recipePool = {}
---creates a recipe registry pool
---@return table reciperegistry pool with the same functions as a normal recipe registry
function recipePool.new()
    local fncTable = {}
    local curRegistry = 1
    local registries = {peripheral.find("recipeRegistry")}
    if #registries == 0 then
        error("no recipe registries found")
    end
    --automaticly add functions to fncTable and override to iterate through each registry in turn
    for k,_ in pairs(registries[1]) do
        fncTable[k] = function (...)
            while true do
                local retVals = {registries[curRegistry][k](...)}
                if retVals[2] ~= nil and retVals[2] == "queryRegistry is on cooldown" then
                    --print("waiting for registry cooldown", curRegistry)
                    sleep(1)
                else
                    curRegistry = curRegistry + 1
                    if curRegistry > #registries then
                        curRegistry = 1
                    end
                    return unpack(retVals)
                end
            end
        end
    end
    return fncTable
end

--enum that maps to recipe entry,
--used to ensure compatibility should recipe format change entry name
local entries = {
    ["inputs"] = "input",
    ["outputs"] = "output",
}

---checks if item entry in a recipe is ok
---@param itemEntryToCheck table recipe.inputs[n] or recipe.output from getAllRecipesForType()
---@return integer verifyRetCodes enum
local function verifyItemEntry(itemEntryToCheck)
    for name, value in pairs(itemEntryToCheck) do
        local checkers = fieldsCheckers[name]
        if checkers ~= nil then
            for _, checker in pairs(checkers) do
                local checkResult = checker(value)
                if checkResult ~= verifyRetCodes.ok then
                    return checkResult
                end
            end
            return verifyRetCodes.ok
        end
    end
    return verifyRetCodes.noItem
end

---verify inputs for a recipe to be not air and contain a valid item or tag
---@param recipe table recipe from recipeRegistry.getAllRecipesForType()
---@param entryToVerify string entries enum
---@return integer verifyRetCodes enum
local function verifyRecipeItems(recipe, entryToVerify)
    if getTableLength(recipe[entryToVerify]) == 0 then
        return verifyRetCodes.noItem
    end
    local noItems = true
    for _, item in pairs(recipe[entryToVerify]) do
        -- well, this allowed sometimes, soe just skeep validation not for tables
        if type(item) == "table" then
            local retCode = verifyRetCodes.ok
            if item["ingredient"] ~= nil then
                retCode = verifyItemEntry(item["ingredient"])
            elseif item["variants"] ~= nil then
                for _, variant in pairs(item["variants"]) do
                    retCode = verifyItemEntry(variant)
                    if retCode ~= verifyRetCodes.ok then
                        break
                    end
                end
            else
                retCode = verifyItemEntry(item)
            end
            if retCode == verifyRetCodes.ok then
                noItems = false
            elseif retCode ~= verifyRetCodes.noItem then
                return retCode
            end
        end
    end
    if noItems then
        return verifyRetCodes.noItem
    end
    return verifyRetCodes.ok
end

---cheks if a recipe is a valid recipe
---@param recipe table recipe as output from recipeRegistry.GetAllRecipesForType()
---@return boolean success
---@return string | nil message on succes nothing is returned, on failure reason for failure is returned
local function verifyRecipe(recipe)
    local inputRet = verifyRetCodes.noEntries
    local outputRet = verifyRetCodes.noEntries
    if recipe[entries.inputs] ~= nil then
        inputRet = verifyRecipeItems(recipe, entries.inputs)
    end
    if recipe[entries.outputs] ~= nil then
        outputRet = verifyRecipeItems(recipe, entries.outputs)
    end
    local retMessage = ""
    local recipeOk = true
    if inputRet == verifyRetCodes.noItem then
        retMessage = retMessage .. "No input item, "
        recipeOk = false
    elseif inputRet == verifyRetCodes.isAir then
        retMessage = retMessage .. "Input is air, "
        recipeOk = false
    elseif inputRet == verifyRetCodes.noEntries then
        retMessage = retMessage .. "No inputs, "
        recipeOk = false
    end
    if outputRet == verifyRetCodes.noItem then
        retMessage = retMessage .. "No output item"
        recipeOk = false
    elseif outputRet == verifyRetCodes.isAir then
        retMessage = retMessage .. "Output is air"
        recipeOk = false
    elseif outputRet == verifyRetCodes.noEntries then
        retMessage = retMessage .. "No outputs"
        recipeOk = false
    end
    if recipeOk == false then
        return recipeOk, retMessage
    end
    return recipeOk
end

local function io_write(...)
    io.write(...)
    io.write("\n")
end

---------------------------------------- MAIN -----------------------------------------
local function run(mask, onlyErrors)
    local registryPool = recipePool.new()
    local craftingHandlers = registryPool.getRecipeTypes()

    local fileHand = io.open("logs.txt", "w")
    io.output(fileHand)

    local logLevel = logging.LEVELS.WARN
    if onlyErrors then
        logLevel = logging.LEVELS.ERROR
    end

    local _log = logging.create("checker", {stdout = io_write, level = logLevel})

    if mask == "*" then
        mask = nil
    end

    for _, handlerName in ipairs(craftingHandlers) do
        if (not IGNORE_LIST[handlerName] and (mask == nil or string.find(handlerName, "^" .. mask) ~= nil)) then
            print("current craftingHandler: " .. handlerName)
            local compactParser = recipeCompact.extraParsers[handlerName]
            -- well, if compactParser will be nil, is just will not be used so
            -- everything just as planned
            local recipes = registryPool.getAllRecipesForType(handlerName, compactParser)
            local endRecipe = recipesToCheck
            if #recipes < endRecipe then
                endRecipe = #recipes
            end
            if endRecipe ~= 0 then
                local corruptedCount = 0
                local corruptMessages = {}
                for i = 1, endRecipe do
                    local recipeIsOk, message = verifyRecipe(recipes[i])
                    if not recipeIsOk then
                        _log:warn("problem with " .. handlerName .. " recipe " .. recipes[i].id .. " reason, " .. message)
                        corruptedCount = corruptedCount + 1
                        corruptMessages[message] = true
                    end
                end
                if corruptedCount * 3 > endRecipe then
                    local buffer = ""
                    for name, _ in pairs(corruptMessages) do
                        buffer = buffer .. " " .. name .. ","
                    end
                    _log:error("Global problem with " .. handlerName .. " more then 1/3 recipes are corrupted. Problems " .. buffer)
                end
            else
                _log:error("problem with " .. handlerName .. " reason, No recipes")
            end
        end
    end

    io.close(fileHand)
end

return {
    run = run
}