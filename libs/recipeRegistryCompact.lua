local extraParsers = {
    ["cyclic:melter"] = { input = {"getIngredients"}, output = {"getRecipeFluid"}, extra = {special = "isSpecial"} },
    ["minecraft:infusion"] = { input = {"getIngredients"}, output = {"getResultItem"}, extra = {mana = "getManaUsage"} },
    -- for this recipe, output of mjoellnir recipe are corrupter by mod authors
    -- and prefix also corrupted by thems.
    ["minecraft:rune_ritual"] = { input = {"getInputs"}, output = {"getResultItem"}, extra = {mana = "getMana"} },
    -- just soil registry, basically
    ["botanypots:soil"] = { input = {"getIngredient"}, output = {}, extra = {growthModifier = "getGrowthModifier"}},
    -- this recipes demostrate deep refactoring usage (by default, max refactoring deep is two)
    -- this is impossible to use inspect in this case, so blame mod developers and inspect code
    -- for example, for this recipe code can be found here:
    -- https://github.com/Darkhax-Minecraft/BotanyPots/blob/1.16.5/src/main/java/net/darkhax/botanypots/crop/CropInfo.java
    ["botanypots:crop"] = { input = {"getSeed"}, output = {"getResults.getItem", "getResults.getChance"}},
    -- just firtilizer registry, basically
    ["botanypots:fertilizer"] = { input = {"getIngredient"}, output = {}},
}

return {
    extraParsers = extraParsers,
    updateIgnoreList = function (ignoreList)
        ignoreList["botanypots:fertilizer"] = true
        ignoreList["botanypots:soil"] = true
        ignoreList["appliedenergistics2:entropy"] = true
    end
}
